\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{rsw}

\LoadClass[a4paper]{article}

\RequirePackage[table]{xcolor}
\RequirePackage{colours}
\RequirePackage{pstricks, fontspec, longtable, booktabs, fancyhdr, draftwatermark, transparent, polyglossia, array, changepage, colortbl, enumitem, ifthen, titlesec}
\RequirePackage[autostyle]{csquotes}
\RequirePackage[a4paper, textwidth=6.5in, textheight=9.2in, footskip=24pt, voffset=0.8in, headheight=72pt]{geometry}
\RequirePackage[unicode, bookmarks, colorlinks, urlcolor=RSW]{hyperref}

\RequirePackage{actiepunten}
\aplsectioncolour{RSW}
\aplsectionfont{\large\bfseries}
\aplheadercolour{RSW!63!white}
\aplrowcolours{RSW!27!white}
\aplhlines{none}
\aplvlines{first}

\setdefaultlanguage{dutch}

\setromanfont [Ligatures={Common}, Scale=1.0]{Noto Sans}
\setsansfont [Ligatures={Common}, Scale=1.0]{Noto Sans}
\setmathrm [Ligatures={Common}, Scale=1.0]{Noto Sans}
\setmonofont [Ligatures={Common}, Scale=1.0]{Noto Sans}

\fancyhead{}
\fancyfoot{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\fancyhead[C]
{ \begin{pspicture}(0,2.4)(20,0)%
    \rput[r](17, 1.2){\includegraphics[scale=0.4]{rsw.eps}}
      \rput[r](16, -0.4){\Large\itshape\color{RSW!73!white}mediateam}
  \end{pspicture}}

\fancyfoot[R]
{ \begin{pspicture}(0,0.6)(20,0)%
    \rput[r](18.4,0){\Large\color{RSW}\Roman{page}}
  \end{pspicture}}

\pagestyle{fancy}

\SetWatermarkAngle{0}
\SetWatermarkHorCenter{6in}
\SetWatermarkVerCenter{10.5in}
\SetWatermarkText{\includegraphics[scale=2]{rsw-wm.eps}}

\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt}

\setlist[itemize]{label=--, before=\let\item\olditem}
\setlist[enumerate,1]{label=\textbf{\color{RSW}\arabic*)}, leftmargin=4.5mm, itemsep=3pt, before=\let\item\bitem}
\setlist[enumerate,2]{label=\textbf{\color{RSW}\alph*)}, leftmargin=5mm, itemsep=3pt}

\let\olditem\item
\def\bitem#1{\olditem \textbf{\color{RSW}#1}}

\def\minihead#1%
{ { \fontsize{6pt}{8pt}\bfseries \expandafter\MakeUppercase\expandafter{#1} \\ } }

\def\adrcat#1%
{ \textit{#1} \\ }

\def\adr#1#2%
{ \hspace*{4mm} #1 \ifthenelse{\equal{#2}{}}{}{(#2)} \\}

\def\uc#1{\uppercase{#1}}

\newcommand{\defDocument}[2]
{ \def\onderwerp
  { #1 }

  \def\aan
  { #2 }

  \def\maketitle
  { \mbox{}
    \vspace{-0.6cm}
    \mbox{}

    \minihead{datum}
    \today \\

    \minihead{onderwerp}
    \textbf{\onderwerp} \\

    \minihead{aan}
    \aan
    \vspace{2mm} } }

\newcommand{\defNotulen}[5]
{ \def\onderwerp
  { Verslag #1 #2 }

  \def\aan
  { #5 }

  \def\maketitle
  { \mbox{}
    \vspace{-0.6cm}
    \mbox{}

    \minihead{datum}
    \today \\

    \minihead{onderwerp}
    \textbf{\onderwerp} \\

    \minihead{aan}
    \aan
    \vspace{2mm}

  \textbf{\uc#1 #2, #3 #4} } }


\titleformat{\section}
  {\normalfont\large\bfseries\color{RSW}}{\thesection}{1em}{}[{\titlerule[0.8pt]}]

\titleformat{\subsection}
  {\normalfont\bfseries\color{RSW}}{\thesubsection}{1em}{}[]
