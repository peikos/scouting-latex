\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{stafoverleg}

\LoadClass[a4paper]{article}

\RequirePackage[table]{xcolor}
\RequirePackage{pstricks, fontspec, longtable, booktabs, fancyhdr, draftwatermark, transparent, polyglossia, array, changepage, colortbl, enumitem, ifthen}
\RequirePackage[autostyle]{csquotes}
\RequirePackage[a4paper, textwidth=6.5in, textheight=8.2in, footskip=69pt, voffset=0.8in, headheight=72pt]{geometry}
\RequirePackage[unicode, bookmarks, colorlinks, urlcolor=black, linkcolor=black, citecolor=black]{hyperref}

\RequirePackage{actiepunten}
\aplsectioncolour{black}
\aplsectionfont{\bfseries}
\aplheadercolour{black!30}
\aplrowcolours{white}
\aplhlines{all}
\aplvlines{all}

\setdefaultlanguage{dutch}

\setromanfont [Ligatures={Common}, Numbers=OldStyle, Scale=1.0]{Cambria}
\setsansfont [Ligatures={Common}, Numbers=OldStyle, Scale=1.0]{Cambria}
\setmathrm [Ligatures={Common}, Numbers=OldStyle, Scale=1.0]{Cambria}
\setmonofont [Ligatures={Common}, Numbers=OldStyle, Scale=1.0]{Cambria}

\fancyhead{}
\fancyfoot{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\fancypagestyle{first}
{ \fancyhead[C]
  { \begin{pspicture}(0,2.4)(20,0)%
      \rput[l](7.5, 1.2){\includegraphics[scale=1.4]{rcg-zw.eps}}
    \end{pspicture}}
  \fancyfoot[C]{\small \fpfooter} }

\fancyhead[C]
{ \begin{pspicture}(0,2.4)(20,0)%
    \rput[l](8, 1.2){\includegraphics[scale=1.4]{rcg-zw.eps}}
  \end{pspicture}}
\fancyfoot[C]
{ \begin{pspicture}(0,2.4)(20,0)%
    \rput[l](8.5, 0.4){\fontspec{Mercury Bold} \itshape \LARGE rover crofts groep }
    \rput[l](8.5, 0){\fontspec{Mercury Bold} \itshape \large bilthoven }
  \end{pspicture}}

\pagestyle{fancy}
\thispagestyle{first}

\SetWatermarkAngle{0}
\SetWatermarkHorCenter{6in}
\SetWatermarkVerCenter{8.4in}
\SetWatermarkText{\includegraphics[scale=4]{rcg-wm.eps}}

\setlength{\parindent}{0pt}

\setlist[itemize]{label=--, before=\let\item\olditem}
\setlist[enumerate,1]{label=\textbf{\arabic*)}, ref=\arabic*, leftmargin=4.5mm, itemsep=3pt, before=\let\item\bitem}
\setlist[enumerate,2]{label=\textbf{\alph*)}, ref=\alph*, leftmargin=5mm, itemsep=3pt}
\setlist[enumerate,3]{label=\textbf{\roman*)}, ref=\roman*, leftmargin=5mm, itemsep=3pt}

\let\olditem\item
\def\bitem#1{\olditem \textbf{#1}}

\def\minihead#1%
{ { \fontsize{6pt}{8pt}\bfseries \expandafter\MakeUppercase\expandafter{#1} \\ } }

\def\adrcat#1%
{ \textit{#1} \\ }

\def\adr#1#2%
{ \hspace*{4mm} #1 \ifthenelse{\equal{#2}{}}{}{(#2)} \\}

\def\uc#1{\uppercase{#1}}

\newcommand{\defNotulen}[5]
{ \def\onderwerp
  { Verslag #1 #2 }

  \def\aan
  { #5 }

  \def\maketitle
  { \mbox{}
    \vspace{-0.6cm}
    \mbox{}

    \begin{minipage}[t]{8.25cm}
      \minihead{datum}
      \today \\

      \minihead{onderwerp}
      \textbf{\onderwerp} \\

      \minihead{aan}
      \aan
      \vspace{2mm}
    \end{minipage}%
    \begin{minipage}[t]{12cm}
      {\fontspec{Mercury Bold} \itshape { \LARGE rover crofts groep } \\ { \large bilthoven } } \\
      \begin{minipage}[t]{4cm}
        \minihead{contactadres}
        \secretariaat
      \end{minipage}%
      \begin{minipage}[t]{5cm}
        \minihead{bezoekadres}
        Troephuis -- Ruysdaellaan 26 \\
        3723 CC Bilthoven \\[1mm]
        \minihead{internet}
        secretariaat@rover-crofts.nl \\
        www.rover-crofts.nl
      \end{minipage}
    \end{minipage}

  \textbf{\uc#1 #2, #3 #4} } }

\def\defDocument#1{\@ifnextchar[{\defDocument@i#1}{\defDocument@i#1[\today]}} % If geen versie: \today
\def\defDocument@i#1[#2]{\@ifnextchar[{\defDocument@ii{#1}[#2]}{\defDocument@ii{#1}[#2][]}} % If geen geadresseerden: {}
\def\defDocument@ii#1[#2][#3]%
{ \def\onderwerp
  { #1 }

  \def\maketitle
  { \mbox{}
    \vspace{-0.6cm}
    \mbox{}

    \begin{minipage}[t]{8.25cm}
      \minihead{onderwerp}
      \textbf{\onderwerp} \\

      \minihead{versie}
      \ifthenelse{\equal{#2}{}}{\today}{#2} \\

      \ifthenelse{\equal{#3}{}}{}
      { \minihead{aan}
        #3 }
      \vspace{2mm}
    \end{minipage}%
    \begin{minipage}[t]{12cm}
      {\fontspec{Mercury Bold} \itshape { \LARGE rover crofts groep } \\ { \large bilthoven } } \\
      \begin{minipage}[t]{4cm}
        \minihead{contactadres}
        \secretariaat
      \end{minipage}%
      \begin{minipage}[t]{5cm}
        \minihead{bezoekadres}
        Troephuis -- Ruysdaellaan 26 \\
        3723 CC Bilthoven \\[1mm]
        \minihead{internet}
        secretariaat@rover-crofts.nl \\
        www.rover-crofts.nl\\[4mm]
      \end{minipage}
    \end{minipage}

  \textbf{} } }

\raggedright
