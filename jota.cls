\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{jota}

\LoadClassWithOptions{article}

\RequirePackage[table]{xcolor}
\RequirePackage{colours}
\RequirePackage{pstricks}
\RequirePackage[normalem]{ulem}
\RequirePackage[dutch]{babel}
\RequirePackage[autostyle]{csquotes}
\RequirePackage[a4paper, textwidth=6.5in, textheight=9in, marginparsep=7pt, marginparwidth=.6in]{geometry}
\RequirePackage[unicode, bookmarks, colorlinks, breaklinks, urlcolor=TangoPlum]{hyperref}
\RequirePackage{epsfig,xkeyval,pdfpages,paralist,fontspec,xunicode,sectsty,fancyhdr,eso-pic, multirow, fp, graphicx, type1cm, wrapfig, calc, ifthen, multicol, framed, rotating, soul, xparse}

\def\bannerType{Algemeen}
\def\prejota{false}
\def\jaartal{\the\year}

\setlength\parindent{0in}
\defaultfontfeatures{Mapping=tex-text}

\setromanfont [Ligatures={Common}, Numbers=OldStyle]{Open Sans Light}
\setsansfont [Ligatures={Common}, Numbers=OldStyle]{Open Sans Light}
\setmathrm [Ligatures={Common}, Numbers=OldStyle]{Open Sans Light}
\setmonofont[Scale=0.8]{Envy Code R}

\newcommand{\defNotulenDatum}[6]
{ \def\headA{Verslag #1}
  \def\headB{#2, #3}
  \def\maketitle
  { \hsection{DATUM}
  #6 \\[.5cm]

  \hsection{AAN}
  #5\\[.5cm]

  \textbf{Verslag #1 #2, #3}\\
  \textbf{Aanwezig:} #4\\ }}

\newcommand{\defNotulenZonderAanwezigen}[4]
{ \def\headA{Verslag #1}
  \def\headB{#2, #3}
  \def\maketitle
  { \hsection{DATUM}
  \today \\[.5cm]

  \hsection{AAN}
    #4\\[.5cm]

  \textbf{Verslag #1 #2, #3}\\ }}

\newcommand{\defNotulen}[5]
{ \def\headA{Verslag #1}
  \def\headB{#2, #3}
  \def\maketitle
  { \hsection{DATUM}
    \today \\[.5cm]

    \hsection{AAN}
      #5\\[.5cm]

    \textbf{Verslag #1 #2, #3}\\
    \textbf{Aanwezig:} #4\\ }}

\newcommand{\defDocument}[1]
{ \def\headA{#1}
  \def\headB{Laatste update: \today}
  \def\pageNumbers{on}
  \def\thefootnote{\fnsymbol{footnote}}
  \def\maketitle
  {\ \vspace{-10mm}\ }}

\newcommand{\defAgenda}[3]
{ \def\headA{Verslag #1}
  \def\headB{#2, #3}
  \def\maketitle
  { \hsection{DATUM}
  \today \\[.5cm]

  \textbf{Agenda #1 \lowercase{#2}, #3}\\ }}

\newcommand{\defVerslag}[4]
{ \def\headA{Verslag #1}
  \def\headB{#2, #3}
  \def\maketitle
  { \hsection{DATUM}
  \today \\[.5cm]

  \hsection{AAN}
  #4\\[.5cm]

  \textbf{Verslag #1 \lowercase{#2}, #3}\\ }}

\newcommand{\kop}[1]{\vspace{2\baselineskip}\noindent\fontspec{Open Sans Bold}\normalsize#1\\[1mm]\normalfont\small}

\newenvironment{tabel}[1]
{ \rowcolors{1}{RowColour}{}
  \begin{tabular}{#1} }
{ \end{tabular}
  \rowcolors{0}{}{}}

\def\pageNumbers{on}

\sectionfont{\fontspec{Muli-Light}\mdseries\upshape\Large}
\subsectionfont{\fontspec{Muli-Light}\mdseries\upshape\large}
\subsubsectionfont{\fontspec{Muli-Light}\mdseries\upshape}

\renewcommand{\labelitemii}{--}
\renewcommand{\labelitemiii}{---}

\pagestyle{fancy}
\thispagestyle{fancy}
\fancyhead{}
\fancyfoot{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\clubpenalty=3000
\widowpenalty=3000

\setlength\headheight{130pt}
\setlength\footskip{75pt}
\setlength\voffset{-1in}
\setlength{\skip\footins}{5mm}

\newcommand{\makeBanner}
{\rput[bl](12.5,1.8){\includegraphics[width=6.25cm]{\bannerType .eps}}%
  \rput[bl](13,2.3){\fontspec{Muli} \color{white}  \ifthenelse{\equal{\prejota}{true}}{Pre-JOTA}{JOTA} \jaartal
                    \ifthenelse{\equal{\bannerType}{Algemeen}}{}{\raisebox{1pt}{\ $\diamond$}\ \bannerType}}}%

\fancyhead[C]
{ \begin{pspicture}(0,3)(20,0)%
    \rput[tr](12.2,3.1){\fontspec{Muli-Light} \huge \headA}%
    \rput[tr](12,2.3){\fontspec{Muli-Light} \large \headB}%
   \makeBanner%
  \end{pspicture}}

\fancyfoot[C]
{ \begin{pspicture}(0,3)(20,0)
  \ifthenelse{\equal{\pageNumbers}{on}}{\rput[tr](18.5,1){\color{black!20} \footnotesize Pagina \thepage}}{}
  \end{pspicture}}

\ifx\nowatermark\undefined
\AddToShipoutPicture{
\put(-5,-100){
\parbox[b][\paperheight]{\paperwidth}{%
\vfill
\centering
\includegraphics[width=0.84\paperwidth]{jota-wm.eps}%
\vfill }}}
\fi

\newcommand{\hsection}[1]{\fontspec{Muli}\tiny #1 \normalfont\normalsize \\}
