\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{verkenners}

\LoadClassWithOptions{article}

\RequirePackage[table]{xcolor}
\RequirePackage{colours}
\RequirePackage{pstricks}
\RequirePackage[normalem]{ulem}
\RequirePackage[dutch]{babel}
\RequirePackage[autostyle]{csquotes}
\RequirePackage[a4paper, textwidth=6.5in, textheight=9in, marginparsep=7pt, marginparwidth=.6in]{geometry}
\RequirePackage[unicode, bookmarks, colorlinks, breaklinks, urlcolor=TangoPlum]{hyperref}
\RequirePackage{xkeyval,pdfpages,paralist,fontspec,xunicode,sectsty,fancyhdr,eso-pic, multirow, fp, graphicx, type1cm, wrapfig, calc, ifthen, multicol, framed, rotating, array}

\renewcommand{\arraystretch}{1.8}

\setlength\parindent{0in}
\defaultfontfeatures{Mapping=tex-text}

\setromanfont [Ligatures={Common}, Numbers=OldStyle]{Open Sans}
\setmathrm [Ligatures={Common}, Numbers=OldStyle]{Open Sans}
\setmonofont[Scale=0.8]{Envy Code R}
\setsansfont [Ligatures={Common}, Numbers=OldStyle]{Open Sans}

\newcommand{\defNotulen}[5]
{ \def\headA{Verslag \lowercase{#1}}
  \def\headB{#2 #3, #4}
  \def\maketitle
  {
  \hsection{DATUM}
  \today \\[.5cm]

  \hsection{AAN}
  #5\\[.5cm]

  \textbf{Verslag \lowercase{#1, #2}, #3 #4}\\ } }

\newcommand{\defDocument}[1]
{ \def\headA{#1}
  \def\headB{Laatste update: \today}
  \def\pageNumbers{on}
  \def\thefootnote{\fnsymbol{footnote}}
  \def\maketitle
  {\ \vspace{-10mm}\ }}

\newcommand{\defVerslag}[4]
{ \def\headA{Verslag \lowercase{#1}}
  \def\headB{#2, #3}
  \def\maketitle
  {
  \hsection{DATUM}
  \today \\[.5cm]

  \hsection{AAN}
  #4\\[.5cm]

  \textbf{Verslag \lowercase{#1 #2}, #3}\\ }}

\newcommand{\kop}[1]{\vspace{2\baselineskip}\noindent\fontspec{Frutiger LT Std 65 Bold}\normalsize#1\\[1mm]\normalfont\small}


\newenvironment{tabel}[1]
{ \rowcolors{1}{RowColour}{}
  \begin{tabular}{#1} }
{ \end{tabular}
  \rowcolors{0}{}{}}


\def\pageNumbers{on}

\sectionfont{\fontspec{Vegur Light}\mdseries\upshape\Large}
\subsectionfont{\fontspec{Vegur Light}\mdseries\upshape\large}
\subsubsectionfont{\fontspec{Vegur Light}\mdseries\upshape}

\renewcommand{\labelitemii}{--}
\renewcommand{\labelitemiii}{---}

\pagestyle{fancy}
\thispagestyle{fancy}
\fancyhead{}
\fancyfoot{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\clubpenalty=3000
\widowpenalty=3000

\setlength\headheight{130pt}
\setlength\footskip{75pt}
\setlength\voffset{-1in}
\setlength{\skip\footins}{5mm}

\fancyhead[C]{
\begin{pspicture}(0,3)(20,0)
  \rput[tr](18.2,2){\fontspec{Vegur Light} \Huge \headA}
  \rput[tr](18,.8){\fontspec{Vegur Light} \large \headB}
\end{pspicture} }

\fancyfoot[C]{
\begin{pspicture}(0,3)(20,0)
\ifthenelse{\equal{\pageNumbers}{on}}{\rput[tr](18.5,1){\color{black!20} \footnotesize Pagina \thepage}}{}
\end{pspicture} }

\ifx\nowatermark\undefined
\AddToShipoutPicture{
\put(-5,-100){
\parbox[b][\paperheight]{\paperwidth}{%
\vfill
\centering
\includegraphics[width=0.80\paperwidth]{rcg-wm}%
\vfill }}}
\fi

\newcommand{\hsection}[1]{\fontspec{Vegur}\tiny #1 \normalfont\normalsize \\}

\rowcolors{2}{white}{gray!10}

\newenvironment{opkomstenschema}
{ \hspace{-5mm}\begin{tabular}{p{1.5cm} p{5cm} p{3cm} p{6cm}}
    \hline
    \rowcolor{gray!35}
    \bf Wanneer & \bf Wat & \bf Wie & \bf Bijzonderheden \\
    \hline }
{ \hline
  \end{tabular} }

\newcommand{\opkomst}[5]
{ \footnotesize #1 #2 & \parbox{5cm}{\footnotesize #3} & \footnotesize #4 & \footnotesize #5 \\ }
