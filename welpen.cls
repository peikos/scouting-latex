% Welpen.cls
%
%    Copyright 2009-2010 Brian van der Bijl
% 
% Agenda's, Notulen, Opkomstenschema's welpen

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{welpen}

\LoadClassWithOptions{article}

\RequirePackage{rcg}
\RequirePackage[normalem]{ulem}
\RequirePackage[dutch]{babel}
\RequirePackage[babel]{csquotes}
\RequirePackage[a4paper, textwidth=6.5in, textheight=9in, marginparsep=7pt, marginparwidth=.6in]{geometry}
\RequirePackage[unicode, bookmarks, colorlinks, breaklinks, 
                urlcolor=TangoPlum,
                pdftitle={},
                pdfauthor={Sahi}]
               {hyperref}
\RequirePackage{xkeyval,pdfpages,paralist,fontspec,xunicode,sectsty,fancyhdr,eso-pic, multirow, fp, graphicx, type1cm, wrapfig, calc, pstricks, pdftricks, ifthen, multicol, framed, rotating, datetime, ragged2e, epsfig, tikz}

\author{Sahi}


\setlength\parindent{0in}
\defaultfontfeatures{Mapping=tex-text}

\setromanfont [Ligatures={Common}, Numbers=OldStyle]{Vegur Light}
\setmathrm [Ligatures={Common}, Numbers=OldStyle]{Vegur Light}
\setmonofont[Scale=0.8]{Envy Code R} 
\setsansfont [Ligatures={Common}, Numbers=OldStyle]{Vegur Light}

\newcommand{\defDraft}{\AddToShipoutPicture{%
  \AtTextCenter{%
    \makebox(0,0){\vspace{4cm}\rotatebox{45}{\textcolor[gray]{0.95}%
{\fontsize{4cm}{4cm}\fontspec{Gotham Book}CONCEPT}}}%
    }%
}}

\def\ShowDate{on}
\def\noDate{\def\ShowDate{off}}

\newcommand{\defNotulen}[5][\welpen]
{ \def\headA{Verslag \lowercase{#2}}
  \def\headB{#3, #4}
  \def\maketitle
  {
  \hsection{DATUM}
  \today \\[.5cm]

  \hsection{AAN} % Welpen
  #1 \\[.5cm]

  \textbf{Verslag \lowercase{#2 #3}, #4}\\
  \textbf{Aanwezig:} #5\\
  }
}

\newcommand{\defAgenda}[4][\welpen]
{ \def\headA{Agenda \lowercase{#2}}
  \def\headB{#3, #4}
  \def\maketitle
  {
  \hsection{DATUM}
  \today \\[.5cm]

  \hsection{AAN} % Welpen
  #1 \\[.5cm]

  \textbf{Agenda \lowercase{#2 #3}, #4}\\
  }
}

\newcommand{\defOpkomsten}[1]
{ \def\headA{Opkomstenschema #1}
  \def\headB{Versie: \today}
  \def\pageNumbers{off}
  \def\maketitle
  {\ \vspace{.5cm}\ 

  }
}

\newcommand{\defTodo}
{ \def\headA{Actiepuntenlijst}
  \def\headB{Versie: \today}
  \def\pageNumbers{off}
  \def\maketitle
  {\ \vspace{.5cm}\ 

  }
}

\newcommand{\defLedenlijst}
{ \def\headA{Ledenlijst Welpen}
  \def\headB{Versie: \today, \currenttime}
  \def\pageNumbers{on}
  \def\emph{\textbf}
  \def\maketitle
  {\ \vspace{.5cm}\ 

  }
}

\newcommand{\defCalamiteitenmap}
{ \def\headA{Calamiteitenmap Welpen}
  \def\headB{Versie: \today, \currenttime}
  \def\pageNumbers{off}
  \def\emph{\textbf}
  \def\maketitle
  {\ \vspace{.5cm}\ 

  }
}

\newcommand{\defDocument}[1]
{ \def\headA{#1}
  \def\headB{Laatste update: \today}
  \def\pageNumbers{on}
  \def\thefootnote{\fnsymbol{footnote}}
  \def\maketitle
  {\ \vspace{.5cm}\ 

  }
}

\newcommand{\kop}[1]{\vspace{2\baselineskip}\noindent\fontspec{Frutiger LT Std 65 Bold}\normalsize#1\\[1mm]\normalfont\small}

%\rowcolors*{2}{}{RowColour}

\def\pageNumbers{on}

\sectionfont{\fontspec{Open Sans Light}\mdseries\upshape\Large}
\subsectionfont{\fontspec{Open Sans Light}\mdseries\upshape\large}
\subsubsectionfont{\fontspec{Open Sans Light}\mdseries\upshape}

\newcommand{\actiepuntHeading}[1]{\paragraph{\hspace{2mm}#1}$\,$\\[1mm]\hangindent=6mm}

\newcommand{\tableheading}[1]{\fontspec{Open Sans Medium}\scriptsize#1\normalfont\normalsize}
                                                    
\renewcommand{\labelitemi}{$\mathbin{\vcenter{\hbox{\scalebox{0.5}{$\bullet$}}}}$}
\renewcommand{\labelitemii}{--}
\renewcommand{\labelitemiii}{---}

\pagestyle{fancy}                    
\thispagestyle{fancy}
\fancyhead{}
\fancyfoot{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\clubpenalty=3000
\widowpenalty=3000

\setlength\headheight{130pt}
\setlength\footskip{75pt}
\setlength\voffset{-1in}
\addtolength{\skip\footins}{1pc plus 5pt}
\renewcommand{\tabcolsep}{0mm}

\fancyhead[C]{
  \begin{tikzpicture}[remember picture,overlay,shift=(current page.north west)]
    \node [right] (title) at (2.8,0.35) {\Huge\fontspec{Yanone Kaffeesatz} \headA};
\ifthenelse{\equal{\ShowDate}{on}}{
  \node [right] (subtitle) at (2.8,-0.3) {\large\fontspec{Yanone Kaffeesatz} \headB}}{};
  \end{tikzpicture}
}

\fancyfoot[C]{
  \begin{tikzpicture}[remember picture,overlay,shift=(current page.north west)]
    \node [right] (rcg) at (0,-25.3) {\color{black!30} \footnotesize Welpen Rover Crofts Groep};
    \ifthenelse{\equal{\pageNumbers}{on}}{\node [left] (pagenr) at (21,-25.3) {\color{black!30} \footnotesize Pagina \thepage};}{}
  \end{tikzpicture}
}
    %\node [right] (rcg) at (2.8,10.35) {\color{black!20} \footnotesize Welpen Rover Crofts Groep};
%\rput[tr](18.5,2.5){\color{black!20} \footnotesize }}{}
%\begin{pspicture}(0,3)(20,0)
%\end{pspicture}

\AddToShipoutPicture{
\put(-2,-72){
\parbox[b][\paperheight]{\paperwidth}{%
\vfill
\centering
\includegraphics[width=\paperwidth,height=\paperheight]{welpen}%
\vfill
}}}

\newcommand{\hsection}[1]{\fontspec{Open Sans Medium}\tiny #1 \normalfont\normalsize \\}

\newcounter{maxtodonr}
\setcounter{maxtodonr}{1}
\newcounter{todonr}
\setcounter{todonr}{1}


\newcommand{\gids}{\\\textit{Gids}}
\newcommand{\helper}{\\\textit{Helper}}


\newcommand{\NewTodo}[5]{
  \expandafter\def\csname TODO\arabic{todonr}kenm\endcsname {#1}
  \expandafter\def\csname TODO\arabic{todonr}item\endcsname {#2}
  \expandafter\def\csname TODO\arabic{todonr}name\endcsname {#3}
  \expandafter\def\csname TODO\arabic{todonr}date\endcsname {#4}
  \expandafter\def\csname TODO\arabic{todonr}state\endcsname {#5}
  \addtocounter{todonr}{1}
  \addtocounter{maxtodonr}{1}
  }
  
\newcounter{WelpNr}
\setcounter{WelpNr}{0}

\newcounter{CurWelpNr}
\setcounter{CurWelpNr}{0}

\newcounter{LeidingNr}
\setcounter{LeidingNr}{0}

\newcounter{CurLeidingNr}
\setcounter{CurLeidingNr}{0}

\newcounter{LLTodoNr}
\setcounter{LLTodoNr}{0}

\newcounter{CurLLTodoNr}
\setcounter{CurLLTodoNr}{0}

\newcounter{ZonderDiploma}
\setcounter{ZonderDiploma}{0}

\newcounter{CurZonderDiploma}
\setcounter{CurZonderDiploma}{0}

\newcounter{MetMedicatie}
\setcounter{MetMedicatie}{0}

\newcounter{CurMetMedicatie}
\setcounter{CurMetMedicatie}{0}

\newcounter{MetDieet}
\setcounter{MetDieet}{0}

\newcounter{CurMetDieet}
\setcounter{CurMetDieet}{0}

\newcommand{\AddLLTodo}[1] 
{ \expandafter\edef\csname LLTodo\arabic{LLTodoNr}\endcsname{#1}
  \addtocounter{LLTodoNr}{1}
}

\newcommand{\WelpKanNietZwemmen}[1]
{
  \expandafter\let\csname ZonderDiploma\arabic{ZonderDiploma}\endcsname #1
  \addtocounter{ZonderDiploma}{1}
}

\newcommand{\WelpHeeftMedicatie}[2]
{
  \expandafter\let\csname MetMedicatie\arabic{MetMedicatie}\endcsname #1
  \expandafter\def\csname MetMedicatie\arabic{MetMedicatie} medic\endcsname{#2}
  \addtocounter{MetMedicatie}{1}
}

\newcommand{\WelpHeeftDieet}[2]
{
  \expandafter\let\csname MetDieet\arabic{MetDieet}\endcsname #1
  \expandafter\def\csname MetDieet\arabic{MetDieet} voor\endcsname{#2}
  \addtocounter{MetDieet}{1}
}

\newcommand{\LedenlijstTodo}
{ \ifthenelse{\arabic{LLTodoNr} > 0}
    {\section*{Todo:}\color{Scarlet}}
    {}
  
  \PrintLLTodo
}

\newcommand{\KidsZonderZwemdiploma}
{ \ifthenelse{\arabic{ZonderDiploma} > 0}
    {\section*{De volgende welpen kunnen niet zwemmen:}}
    {}
  
  \PrintNietZwemmer
}

\newcommand{\PrintNietZwemmer}
{
  \csname ZonderDiploma\arabic{CurZonderDiploma}\endcsname \\
  \addtocounter{CurZonderDiploma}{1} 
  \ifthenelse{\arabic{ZonderDiploma} > \arabic{CurZonderDiploma}}
    {\PrintNietZwemmer}
    {}
}

\newcommand{\KidsMetMedicatie}
{ \ifthenelse{\arabic{MetMedicatie} > 0}
    {\section*{De volgende welpen gebruiken medicatie:}}
    {}
  
  \PrintMedicatieKind
}

\newcommand{\PrintMailAdressen}
{ \vfill
  \section*{Mailadressen welpen}
  \setcounter{CurWelpNr}{0}
  \begin{minipage}{.95\textwidth}
    \footnotesize \tt \RaggedRight \PrintWelpMailadres 
  \end{minipage}
  \vspace{15mm}
}

\newcommand{\PrintWelpMailadres}
{ \edef\CurCode{\csname welp\arabic{CurWelpNr}\endcsname}
  \mbox{"\csname welp-\CurCode-naam\endcsname " <\csname welp-\CurCode-mail\endcsname >}
  \addtocounter{CurWelpNr}{1}
  \ifthenelse{\arabic{WelpNr} > \arabic{CurWelpNr}}
  {\PrintWelpMailadres}
  {}}

\newcommand{\PrintMedicatieKind}
{
  \csname MetMedicatie\arabic{CurMetMedicatie}\endcsname :
  \csname MetMedicatie\arabic{CurMetMedicatie} medic\endcsname\\
  \addtocounter{CurMetMedicatie}{1} 
  \ifthenelse{\arabic{MetMedicatie} > \arabic{CurMetMedicatie}}
    {\PrintMedicatieKind}
    {}
}

\newcommand{\KidsMetDieten}
{ \ifthenelse{\arabic{MetDieet} > 0}
    {\section*{De volgende welpen hebben voedselallergiën of andere diëten:}}
    {}
  
  \PrintDieetKind
}

\newcommand{\PrintDieetKind}
{
  \csname MetDieet\arabic{CurMetDieet}\endcsname :
  \csname MetDieet\arabic{CurMetDieet} voor\endcsname\\
  \addtocounter{CurMetDieet}{1} 
  \ifthenelse{\arabic{MetDieet} > \arabic{CurMetDieet}}
    {\PrintDieetKind}
    {}
}

\newcommand{\PrintLLTodo}
{
  \csname LLTodo\arabic{CurLLTodoNr}\endcsname \\
  \ifthenelse{\arabic{LLTodoNr} > \arabic{CurLLTodoNr}}
    {\addtocounter{CurLLTodoNr}{1} \PrintLLTodo}
    {\normalsize\color{black}}
}
  
\newcommand{\BeginWelp}[1]
{ \def\CurNr{}
  \def\CurNaam{}
  \def\CurVoornaam{}
  \def\CurVoorletters{}
  \def\CurAchternaam{}
  \def\CurLNaam{}
  \def\CurVolleNaam{}
  \def\CurGeslacht{}
  \def\CurAdres{}
  \def\CurTel{}
  \def\CurMail{}
  \def\CurGeb{}
  \def\CurLeeftijd{}
  \def\CurBijz{}
  \def\CurMedic{}
  \def\CurAllerg{}
  \def\CurDieet{}
  \def\CurArts{}
  \def\CurVerz{}
  \def\CurPolis{}
  \def\CurDipl{}
  \def\CurKwalif{}
  \def\CurHeim{0}
  \def\CurPlas{0}
  \def\CurBasis{0}
  \def\CurKamp{0}
  \def\CurEHBO{0}
  \def\CurAuto{0}
  \def\CurRol{}
  %\def\CurNest{}
  \def\CurNote{0}
  }

  \def\BeginLeiding{\BeginWelp}

\newcommand{\EindeLeiding}[1]
{ \expandafter\edef\csname leiding\arabic{LeidingNr}\endcsname{#1}
  \addtocounter{LeidingNr}{1}
  \global\expandafter\let\csname leiding-#1-nr\endcsname \CurNr
  \global\expandafter\let\csname leiding-#1-naam\endcsname \CurNaam
  \global\expandafter\let\csname leiding-#1-voornaam\endcsname \CurVoornaam
  \global\expandafter\let\csname leiding-#1-achternaam\endcsname \CurAchternaam
  \global\expandafter\let\csname leiding-#1-lnaam\endcsname \CurLNaam
  \global\expandafter\let\csname leiding-#1-vollenaam\endcsname \CurVolleNaam
  \global\expandafter\let\csname leiding-#1-geslacht\endcsname \CurGeslacht
  \global\expandafter\let\csname leiding-#1-adres\endcsname \CurAdres
  \global\expandafter\let\csname leiding-#1-tel\endcsname \CurTel
  \global\expandafter\let\csname leiding-#1-mail\endcsname \CurMail
  \global\expandafter\let\csname leiding-#1-geb\endcsname \CurGeb
  \global\expandafter\let\csname leiding-#1-rol\endcsname \CurRol
  \global\expandafter\let\csname leiding-#1-leeft\endcsname \CurLeeftijd
  \global\expandafter\let\csname leiding-#1-bijz\endcsname \CurBijz
  \global\expandafter\let\csname leiding-#1-medic\endcsname \CurMedic
  \global\expandafter\let\csname leiding-#1-allerg\endcsname \CurAllerg
  \global\expandafter\let\csname leiding-#1-dieet\endcsname \CurDieet
  \global\expandafter\let\csname leiding-#1-arts\endcsname \CurArts
  \global\expandafter\let\csname leiding-#1-verz\endcsname \CurVerz
  \global\expandafter\let\csname leiding-#1-polis\endcsname \CurPolis
  %\global\expandafter\let\csname leiding-#1-kwalif\endcsname \CurKwalif
  \global\expandafter\let\csname leiding-#1-basis\endcsname \CurBasis
  \global\expandafter\let\csname leiding-#1-kamp\endcsname \CurKamp
  \global\expandafter\let\csname leiding-#1-ehbo\endcsname \CurEHBO
  \global\expandafter\let\csname leiding-#1-auto\endcsname \CurAuto
  \global\expandafter\let\csname leiding-#1-note\endcsname \CurNote
  \ifthenelse{\equal{\CurVerz}{}}{\AddLLTodo{Van #1 ontbreekt de verzekeringsmaatschappij.}}{} 
  \ifthenelse{\equal{\CurPolis}{}}{\AddLLTodo{Van #1 ontbreekt het polisnummer.}}{}
  \ifthenelse{\equal{\CurArts}{}}{\AddLLTodo{Van #1 ontbreekt de huisarts.}}{}
  }

\newcommand{\CalamiteitenMap}
{ \WelpPagina 
  \LeidingPagina }

\newcommand{\PrintLeiding}[2][]{
  \leiding
    {\csname leiding-#2-naam\endcsname\\\textit{\csname leiding-#2-lnaam\endcsname}}
    {\csname leiding-#2-adres\endcsname}
    {\csname leiding-#2-tel\endcsname}
    {\csname leiding-#2-mail\endcsname}
    {\csname leiding-#2-geb\endcsname\\
    \csname leiding-#2-rol\endcsname}
    {\csname leiding-#2-bijz\endcsname}
    {#1}

}

\newcounter{evenodd}

\newcommand{\PrintKwalificaties}
{ \setcounter{CurLeidingNr}{0}
  \setcounter{evenodd}{0}
  \begin{pspicture}(0,0)(16,4.5)
  \rput[lt](.25,2.25){\fontspec{Open Sans Light}\mdseries\upshape\Huge Kwalificaties}
  \rput[lt](8,3){\includegraphics[width=1.8cm]{Leiding}}
  \rput[lt](10,3){\includegraphics[width=1.8cm]{Bivak}}
  \rput[lt](12,3){\includegraphics[width=1.8cm]{EHBO}}
  \rput[lt](14,3){\includegraphics[width=1.8cm]{Auto}}
  \psline[linewidth=.2mm, linecolor=Grey!50](-.5,.95)(16.5,.95)
  \psline[linewidth=.2mm, linecolor=Grey!50](-.5,.95)(-.5,3)
  \end{pspicture}\\
  \PrintLeidingKwalificatie
}

\newcommand{\tleidster}[1]
{ \ifthenelse{\equal{\csname leiding-#1-geslacht\endcsname}{M}}
    {teamleider}
    {teamleidster}}

\newcommand{\leidster}[1]
{ \ifthenelse{\equal{\csname leiding-#1-geslacht\endcsname}{M}}
    {leider}
    {leidster}}

\newcommand{\PrintLeidingKwalificatie}
{ \edef\CurCode{\csname leiding\arabic{CurLeidingNr}\endcsname}
  \ifthenelse{\equal{\value{evenodd}}{1}}{\setcounter{evenodd}{2}}{\setcounter{evenodd}{1}}
  \begin{pspicture}(0,0)(16,2)
    \ifthenelse{\equal{\value{evenodd}}{1}}
    { \psframe[cornersize=absolute,%
               fillstyle=solid,%
               linecolor=RowColourTwo,%
               fillcolor=RowColourTwo](-.5,0.75)(16.5,2.75)}
    { \psframe[cornersize=absolute,%
               fillstyle=solid,%
               linecolor=White,%
               fillcolor=White](-.5,0.75)(16.5,2.75)}
  \rput[l](0, 2){\Large\csname leiding-\CurCode-naam\endcsname}
  \rput[l](0,1.25){\normalsize \csname leiding-\CurCode-lnaam\endcsname ,
                  \itshape
  \ifthenelse{\equal{\csname leiding-\CurCode-rol\endcsname}{Teamleider}}
  {\tleidster{\CurCode}}
  { \ifthenelse{\equal{\csname leiding-\CurCode-rol\endcsname}{Leider}}
    {\leidster{\CurCode}}
    {aspirant}}}
  \rput[lt](8.25,2.25){
  \ifthenelse{\equal{\csname leiding-\CurCode-basis\endcsname}{1}}
  {\includegraphics[width=1cm]{check}}
  {}}
  \rput[lt](10.25,2.25){
  \ifthenelse{\equal{\csname leiding-\CurCode-kamp\endcsname}{1}}
  {\includegraphics[width=1cm]{check}}
  {}}
  \rput[lt](12.25,2.25){
  \ifthenelse{\equal{\csname leiding-\CurCode-ehbo\endcsname}{1}}
  {\includegraphics[width=1cm]{check}}
  {}}
  \rput[lt](14.25,2.25){
  \ifthenelse{\equal{\csname leiding-\CurCode-auto\endcsname}{1}}
  {\includegraphics[width=1cm]{check}}
  {}}

  \end{pspicture}
  \addtocounter{CurLeidingNr}{1}
  \ifthenelse{\arabic{LeidingNr} > \arabic{CurLeidingNr}}
    {\\\PrintLeidingKwalificatie}
    {}}


\newcommand{\LeidingPagina}
{ \pagebreak
  \edef\CurCode{\csname leiding\arabic{CurLeidingNr}\endcsname}

  \begin{pspicture}(0,0)(16,24)
    \rput[r](18.5, 24){\color{black!3}\fontsize{48}{48} \fontspec{OCR A Std} \csname leiding-\CurCode-nr\endcsname}
    \rput[l](0, 22){\Huge\csname leiding-\CurCode-vollenaam\endcsname \,
                    \large (\csname leiding-\CurCode-lnaam\endcsname)}
    
    \rput[l](0, 20){\begin{minipage}{6cm}\textbf{Adres:}\\ \csname leiding-\CurCode-adres\endcsname \end{minipage}}
    \rput[l](0, 18.5){\begin{minipage}{6cm}\textbf{E-mail:}\\ \csname leiding-\CurCode-mail\endcsname \end{minipage}}
    \rput[l](0, 17){\begin{minipage}{6cm}\textbf{Telefoon:}\\ \csname leiding-\CurCode-tel\endcsname \end{minipage}}

    \rput[l](9, 20){\begin{minipage}{6cm}
      \raggedright\textbf{Verzekering:}\\ 
      \ifthenelse{\equal{\csname leiding-\CurCode-verz\endcsname}{}}
        {Verzekeraar en polisnr. onbekend!}
          {\ifthenelse{\equal{\csname leiding-\CurCode-polis\endcsname}{}}
            {\csname leiding-\CurCode-verz\endcsname, Polisnr. onbekend!}
            {\csname leiding-\CurCode-verz\endcsname\ polisnr. \csname leiding-\CurCode-polis\endcsname}}
          \end{minipage}}
        
    \rput[l](9, 18.5){\begin{minipage}{6cm}
      \raggedright\textbf{Huisarts:}\\ 
      \ifthenelse{\equal{\csname leiding-\CurCode-arts\endcsname}{}}
        {Huisarts onbekend!}
        {\csname leiding-\CurCode-arts\endcsname}
      \end{minipage}}
    
    \rput[l](9, 17){\begin{minipage}{6cm}\textbf{Geboortedatum:}\\ \csname leiding-\CurCode-geb\endcsname \end{minipage}}


    \ifthenelse{\equal{\csname welp-\CurCode-note\endcsname}{1}}
    { \rput[lt](-.5,15.5){\includegraphics[scale=0.825]{Post-it}}}{}
    \rput[lt]{3}(.5,13.5)
    { \begin{minipage}{15cm}
        \fontspec{Notehand Bold}
        \ifthenelse{\equal{\csname welp-\CurCode-note\endcsname}{1}}
        { \section*{\fontspec{Notehand Bold}Bijzonderheden}}{}
        \ifthenelse{\equal{\csname leiding-\CurCode-bijz\endcsname}{}}{}
        {\csname leiding-\CurCode-bijz\endcsname}
        \ifthenelse{\equal{\csname leiding-\CurCode-allerg\endcsname}{}}{}
        {\subsection*{\fontspec{Notehand Bold}Allergiën:}
           \csname leiding-\CurCode-allerg\endcsname}
        \ifthenelse{\equal{\csname leiding-\CurCode-dieet\endcsname}{}}{}
        {\subsection*{\fontspec{Notehand Bold}Diëten:}
           \csname leiding-\CurCode-dieet\endcsname}
      \end{minipage}
    }
  \rput[rt](10,2.5){
  \ifthenelse{\equal{\csname leiding-\CurCode-rol\endcsname}{Teamleider}}
  {\includegraphics[width=1.8cm]{TL}}
  { \ifthenelse{\equal{\csname leiding-\CurCode-rol\endcsname}{Leider}}
    {\includegraphics[width=1.8cm]{L}}
    {\includegraphics[width=1.8cm]{A}}}}
  \rput[rt](12,2.5){
  \ifthenelse{\equal{\csname leiding-\CurCode-basis\endcsname}{1}}
  {\includegraphics[width=1.8cm]{Leiding}}
  {\includegraphics[width=1.8cm]{LeidingX}}}
  \rput[rt](14,2.5){
  \ifthenelse{\equal{\csname leiding-\CurCode-kamp\endcsname}{1}}
  {\includegraphics[width=1.8cm]{Bivak}}
  {\includegraphics[width=1.8cm]{BivakX}}}
  \rput[rt](16,2.5){
  \ifthenelse{\equal{\csname leiding-\CurCode-ehbo\endcsname}{1}}
  {\includegraphics[width=1.8cm]{EHBO}}
  {\includegraphics[width=1.8cm]{EHBOX}}}
  \rput[rt](18,2.5){
  \ifthenelse{\equal{\csname leiding-\CurCode-auto\endcsname}{1}}
  {\includegraphics[width=1.8cm]{Auto}}
  {\includegraphics[width=1.8cm]{AutoX}}}

  \end{pspicture}
  \addtocounter{CurLeidingNr}{1}
  \ifthenelse{\arabic{LeidingNr} > \arabic{CurLeidingNr}}
    {\LeidingPagina}
    {}}



\newcommand{\WelpPagina}
{ \pagebreak
  \edef\CurCode{\csname welp\arabic{CurWelpNr}\endcsname}

  \begin{pspicture}(0,0)(16,24)
    \rput[r](18.5, 24){\color{black!3}\fontsize{48}{48} \fontspec{OCR A Std} \csname welp-\CurCode-nr\endcsname}

    \rput[l](2.5, 22){\Huge\csname welp-\CurCode-vollenaam\endcsname \,
                    \large (\csname welp-\CurCode-leeft\endcsname\ jaar, geboren op \csname welp-\CurCode-geb\endcsname)}

    \rput[tl](-1, 21){\includegraphics[width=6cm, height=7.5cm]{Foto}}
    \rput[tl](0.15, 19.85){\includegraphics[angle=3,width=3.375cm, height=4.875cm]{./Foto/\CurCode}}
    \rput[tl](-1, 21){\includegraphics[width=6cm, height=7.5cm]{Foto-overlay}}

    \rput[l](6, 19){\begin{minipage}{6cm}\textbf{Adres:}\\ \csname welp-\CurCode-adres\endcsname \end{minipage}}
    \rput[l](6, 17.5){\begin{minipage}{6cm}\textbf{E-mail:}\\ \tt \csname welp-\CurCode-mail\endcsname \end{minipage}}
    \rput[l](6, 16){\begin{minipage}{6cm}\textbf{Telefoon:}\\ \csname welp-\CurCode-tel\endcsname \end{minipage}}

    \rput[l](11, 19){\begin{minipage}{6cm}
      \raggedright\textbf{Verzekering:}\\ 
      \ifthenelse{\equal{\csname welp-\CurCode-verz\endcsname}{}}
        {Verzekeraar en polisnr. onbekend!}
          {\ifthenelse{\equal{\csname welp-\CurCode-polis\endcsname}{}}
            {\csname welp-\CurCode-verz\endcsname, Polisnr. onbekend!}
            {\csname welp-\CurCode-verz\endcsname\ polisnr. \csname welp-\CurCode-polis\endcsname}}
          \end{minipage}}
        
    \rput[l](11, 17.5){\begin{minipage}{6cm}
      \raggedright\textbf{Huisarts:}\\ 
      \ifthenelse{\equal{\csname welp-\CurCode-arts\endcsname}{}}
        {Huisarts onbekend!}
        {\csname welp-\CurCode-arts\endcsname}
      \end{minipage}}
    
    \rput[l](11, 16){\begin{minipage}{6cm}
      \raggedright\textbf{Zwemdiploma's:}\\ 
      \ifthenelse{\equal{\csname welp-\CurCode-dipl\endcsname}{}}{Onbekend}
        {\ifthenelse{\equal{\csname welp-\CurCode-dipl\endcsname}{Nee}}{Geen}
          {\csname welp-\CurCode-dipl\endcsname}\\}
      \end{minipage}}

      \pstriangle[fillstyle=solid,%
                  linecolor=Nest\csname welp-\CurCode-nest\endcsname 1,%
                  fillcolor=Nest\csname welp-\CurCode-nest\endcsname 1,](1,21)(2.7,3)
      \ifthenelse{\equal{\csname welp-\CurCode-rol\endcsname}{Gids}}
      {\rput[b](1,21.2){\fontspec[Scale=1.5]{Yanone Kaffeesatz}\Huge\color{Nest\csname welp-\CurCode-nest\endcsname 2} G}}{}
      \ifthenelse{\equal{\csname welp-\CurCode-rol\endcsname}{Helper}}
      {\rput[b](1,21.2){\fontspec[Scale=1.5]{Yanone Kaffeesatz}\Huge\color{Nest\csname welp-\CurCode-nest\endcsname 2} H}}{}
    
    \ifthenelse{\equal{\csname welp-\CurCode-note\endcsname}{1}}
    {\rput[lt](-.5,13){\includegraphics[scale=0.825]{Post-it}}}{}
    \rput[lt]{3}(.5,11)
    { \begin{minipage}{15cm}
      \fontspec{Notehand Bold}
      \ifthenelse{\equal{\csname welp-\CurCode-note\endcsname}{1}}
      { \section*{\fontspec{Notehand Bold}Bijzonderheden}}{}
        \ifthenelse{\equal{\csname welp-\CurCode-bijz\endcsname}{}}{}
        {\csname welp-\CurCode-bijz\endcsname}

        \ifthenelse{\equal{\csname welp-\CurCode-heim\endcsname}{0}}{}
        {{\color{TangoScarletRed}\csname welp-\CurCode-voornaam\endcsname\ heeft (soms) last van heimwee!}}

        \ifthenelse{\equal{\csname welp-\CurCode-plas\endcsname}{0}}{}
        {{\color{TangoScarletRed}\csname welp-\CurCode-voornaam\endcsname\ heeft (soms) last van bedplassen!}}

        \ifthenelse{\equal{\csname welp-\CurCode-dipl\endcsname}{}}
        {{\color{TangoScarletRed}Het is niet bekend of \csname welp-\CurCode-voornaam\endcsname\ kan zwemmen\\}}
          {\ifthenelse{\equal{\csname welp-\CurCode-dipl\endcsname}{Nee}}
          {{\color{TangoScarletRed}\csname welp-\CurCode-voornaam\endcsname\ heeft geen zwemdiploma's!\\}}{}}

        \ifthenelse{\equal{\csname welp-\CurCode-medic\endcsname}{}}{}
        {\subsection*{\fontspec{Notehand Bold}Medicijnen:}
           \csname welp-\CurCode-medic\endcsname}

        \ifthenelse{\equal{\csname welp-\CurCode-allerg\endcsname}{}}{}
        {\subsection*{\fontspec{Notehand Bold}Allergiën:}
           \csname welp-\CurCode-allerg\endcsname}

        \ifthenelse{\equal{\csname welp-\CurCode-dieet\endcsname}{}}{}
        {\subsection*{\fontspec{Notehand Bold}Diëten:}
           \csname welp-\CurCode-dieet\endcsname}
      \end{minipage}
    }
  \end{pspicture}
  \addtocounter{CurWelpNr}{1}
  \ifthenelse{\arabic{WelpNr} > \arabic{CurWelpNr}}
  {\WelpPagina}
    {}}

\newcommand{\EindeWelp}[1]
{ \expandafter\edef\csname welp\arabic{WelpNr}\endcsname{#1}
  \addtocounter{WelpNr}{1}
  \ifthenelse{\equal{\CurDipl}{}}{\def\CurNote{1}}{}
  \global\expandafter\let\csname welp-#1-nr\endcsname \CurNr
  \global\expandafter\let\csname welp-#1-voornaam\endcsname \CurVoornaam
  \global\expandafter\let\csname welp-#1-achternaam\endcsname \CurAchternaam
  \global\expandafter\let\csname welp-#1-naam\endcsname \CurNaam
  \global\expandafter\let\csname welp-#1-vollenaam\endcsname \CurVolleNaam
  \global\expandafter\let\csname welp-#1-geslacht\endcsname \CurGeslacht
  \global\expandafter\let\csname welp-#1-adres\endcsname \CurAdres
  \global\expandafter\let\csname welp-#1-tel\endcsname \CurTel
  \global\expandafter\let\csname welp-#1-mail\endcsname \CurMail
  \global\expandafter\let\csname welp-#1-geb\endcsname \CurGeb
  \global\expandafter\let\csname welp-#1-leeft\endcsname \CurLeeftijd
  \global\expandafter\let\csname welp-#1-bijz\endcsname \CurBijz
  \global\expandafter\let\csname welp-#1-medic\endcsname \CurMedic
  \global\expandafter\let\csname welp-#1-allerg\endcsname \CurAllerg
  \global\expandafter\let\csname welp-#1-dieet\endcsname \CurDieet
  \global\expandafter\let\csname welp-#1-arts\endcsname \CurArts
  \global\expandafter\let\csname welp-#1-verz\endcsname \CurVerz
  \global\expandafter\let\csname welp-#1-polis\endcsname \CurPolis
  \global\expandafter\let\csname welp-#1-dipl\endcsname \CurDipl
  \global\expandafter\let\csname welp-#1-heim\endcsname \CurHeim
  \global\expandafter\let\csname welp-#1-plas\endcsname \CurPlas
  \global\expandafter\let\csname welp-#1-note\endcsname \CurNote
  \ifthenelse{\equal{\CurDipl}{}}{\ifthenelse{\equal{\CurGeslacht}{M}}
  { \AddLLTodo{Van #1 is niet bekend of hij een zwemdiploma heeft.}}
  { \AddLLTodo{Van #1 is niet bekend of zij een zwemdiploma heeft.}}}

  \ifthenelse{\equal{\CurVerz}{}}{\AddLLTodo{Van #1 ontbreekt de verzekeringsmaatschappij.}}{} 
  \ifthenelse{\equal{\CurPolis}{}}{\AddLLTodo{Van #1 ontbreekt het polisnummer.}}{}
  \ifthenelse{\equal{\CurArts}{}}{\AddLLTodo{Van #1 ontbreekt de huisarts.}}{}
  }

\newcommand{\LidNr}[1]
{ \def\CurNr{#1}}

\newcommand{\Naam}[3]
{ \def\CurVoornaam{#1}
  \def\CurAchternaam{#3}
  \ifthenelse{\equal{#2}{}}
    {\def\CurVolleNaam{#1 #3}}
    {\def\CurVolleNaam{#1 #2 #3}}
  \def\CurNaam{#1 #3}}

\newcommand{\LeidingNaam}[1]
{ \def\CurLNaam{#1}
  }

\newcommand{\Geslacht}[1]
{ \def\CurGeslacht{#1}
  }

\newcommand{\Adres}[1]
{ \def\CurAdres{#1}} 

\newcommand{\Telefoon}[1]
{ \def\CurTel{#1}}

\def\Mail{\@ifstar\@Mail\@@Mail}

\newcommand{\@@Mail}[1]
{ \def\CurMail{#1}}

\newcommand{\@Mail}[2]
{ \def\CurMail{#1}
  \def\CurAltMail{#2}} 

\newcommand{\Geboortedatum}[2]
{ \def\CurGeb{#1}
  \def\CurLeeftijd{#2}}

\newcommand{\Rol}[1]
{ \def\CurRol{#1}}

\newcommand{\Bijzonderheden}[1]
{ \def\CurNote{1}
  \def\CurBijz{#1}}

\newcommand{\Kwalificaties}[1]
{ \def\CurKwalif{#1}}

\newcommand{\Medicijnen}[1]
{ \ifthenelse{\equal{#1}{}}{}{\WelpHeeftMedicatie{\CurVoornaam}{#1} \def\CurNote{1}}
  \def\CurMedic{#1}}

\newcommand{\Dieet}[1]
{ \ifthenelse{\equal{#1}{}}{}{\WelpHeeftDieet{\CurVoornaam}{#1 diëet} \def\CurNote{1}}
  \def\CurDieet{#1}}

\newcommand{\Allergie}[2][]
{ \ifthenelse{\equal{#1}{V}}{\WelpHeeftDieet{\CurVoornaam}{Allergisch voor #2} \def\CurNote{1}}{}
  \def\CurAllerg{#2}}

\newcommand{\Huisarts}[1]
{ \def\CurArts{#1}}

\newcommand{\Zwemdiploma}[1]
{ \def\CurDipl{#1}
  \ifthenelse{\equal{#1}{Nee}}{\WelpKanNietZwemmen{\CurNaam} \def\CurNote{1}}{}
  } 

\newcommand{\Heimwee}
{ \def\CurNote{1}
  \def\CurHeim{1}}

\newcommand{\Bedplassen}
{ \def\CurNote{1}
  \def\CurPlas{1}}

\newcommand{\Verzekering}[2]
{ \def\CurVerz{#1}
  \def\CurPolis{#2}}

\newcommand{\EHBO}
{ \def\CurEHBO{1}}

\newcommand{\Rijbewijs}
{ \def\CurAuto{1}}

\newcommand{\KampBivak}
{ \def\CurKamp{1}}

\newcommand{\BasisTraining}
{ \def\CurBasis{1}}

\def\ZetOpTodoLijst{\AddLLTodo}

\newcommand{\NewWelp}[8]{
  \expandafter\def\csname welp-#1-naam\endcsname {#2}
  \expandafter\def\csname welp-#1-adres\endcsname {#3}
  \expandafter\def\csname welp-#1-tel\endcsname {#4}
  \expandafter\def\csname welp-#1-mail\endcsname {#5}
  \expandafter\def\csname welp-#1-geb\endcsname {#6}
  \expandafter\def\csname welp-#1-bijz\endcsname {#7}
  \expandafter\def\csname welp-#1-notes\endcsname {#8}
  }

\def\kleur{Paars}

\newcommand{\PrintWelp}[3][]{
  \global\expandafter\def\csname welp-#2-rol\endcsname{#3}
  \global\expandafter\let\csname welp-#2-nest\endcsname \kleur
  \welp
    {\csname welp-#2-naam\endcsname\\\textit{#3}}
    {\csname welp-#2-adres\endcsname}
    {\csname welp-#2-tel\endcsname}
    {\csname welp-#2-mail\endcsname}
    {\csname welp-#2-geb\endcsname}
    {\csname welp-#2-bijz\endcsname}
    {#1}
  
}

\newcommand{\PrintTodos}[1]{
  \vspace{2.5mm}
  \setcounter{todonr}{1}
  \@ifundefined{c@evenodd}{\newcounter{evenodd}}{}
    \setcounter{evenodd}{2}
    \begin{pspicture}(0,0)(18,0.5)
    \psframe[cornersize=absolute,%
               linearc=0.2,%
               fillstyle=solid,%
               linecolor=RowHeader,%
               fillcolor=RowHeader](0,0)(16.5,0.5)
    \rput[l](.25,0.25){\fontspec{Open Sans Medium}\scriptsize KENMERK}
    \rput[l](2,0.25){\fontspec{Open Sans Medium}\scriptsize WAT}
    \rput[l](9.5,0.25){\fontspec{Open Sans Medium}\scriptsize WIE}
    \rput[l](12,0.25){\fontspec{Open Sans Medium}\scriptsize WANNEER}
    \rput[l](14.5,0.25){\fontspec{Open Sans Medium}\scriptsize STATUS}
    \end{pspicture}
  \ifthenelse{\arabic{maxtodonr} > 1}{\PrintTodo}{}
}

\newcommand{\PrintTodo}{
  \ifthenelse{\equal{\value{evenodd}}{1}}{\setcounter{evenodd}{2}}{\setcounter{evenodd}{1}}
    \begin{pspicture}(0,0)(18,0.5)
    \ifthenelse{\equal{\value{evenodd}}{1}}
    { \psframe[cornersize=absolute,%
               linearc=0.2,%
               fillstyle=solid,%
               linecolor=RowColourTwo,%
               fillcolor=RowColourTwo](0,0)(16.5,0.5)}
    { \psframe[cornersize=absolute,%
               linearc=0.2,%
               fillstyle=solid,%
               linecolor=RowColour,%
               fillcolor=RowColour](0,0)(16.5,0.5)}
    \def\kleur{\ifthenelse
      {\equal{\csname TODO\arabic{todonr}state\endcsname}{afgevoerd}}
      {\color{Grey}}
      {\color{Black}}}        
    \rput[l](.25,0.225){\kleur\small\csname TODO\arabic{todonr}kenm\endcsname}
    \rput[l](2,0.225){\kleur\small\csname TODO\arabic{todonr}item\endcsname}
    \rput[l](9.5,0.225){\kleur\small\csname TODO\arabic{todonr}name\endcsname}
    \rput[l](12,0.225){\kleur\small\csname TODO\arabic{todonr}date\endcsname}
    \rput[l](14.5,0.225){\kleur\small\csname TODO\arabic{todonr}state\endcsname}
    \ifthenelse
      {\equal{\csname TODO\arabic{todonr}state\endcsname}{afgevoerd}}
      {\psline[linewidth=.2mm, linecolor=Grey](-.25,0.225)(16.75,0.225)}
      {}
    \end{pspicture}
     \addtocounter{todonr}{1}
    \ifthenelse{\arabic{todonr} < \arabic{maxtodonr}}{\PrintTodo}{}}


    \newenvironment{opkomstenschema}
    { \@ifundefined{c@evenodd}{\newcounter{evenodd}}{}
      \setcounter{evenodd}{2}
      \begin{pspicture}(0,0)(18,0.5)
      \psframe[cornersize=absolute,%
                 linearc=0.2,%
                 fillstyle=solid,%
                 linecolor=RowHeader,%
                 fillcolor=RowHeader](-.5,0)(17,0.5)
        \rput[l](-.25,0.25){\fontspec{Open Sans Medium}\scriptsize WANNEER}
      \rput[l](2,0.25){\fontspec{Open Sans Medium}\scriptsize WAT}
      \rput[l](6,0.25){\fontspec{Open Sans Medium}\scriptsize WIE}
           \rput[l](9.5,0.25){\fontspec{Open Sans Medium}\scriptsize BIJZONDERHEDEN}
      \end{pspicture}
    }{}

    \newenvironment{opkomstenschemaW}
    { \@ifundefined{c@evenodd}{\newcounter{evenodd}}{}
      \setcounter{evenodd}{2}
      \begin{pspicture}(0,0)(18,0.5)
      \psframe[cornersize=absolute,%
                 linearc=0.2,%
                 fillstyle=solid,%
                 linecolor=RowHeader,%
                 fillcolor=RowHeader](-.5,0)(17,0.5)
        \rput[l](-.25,0.25){\fontspec{Open Sans Medium}\scriptsize WANNEER}
      \rput[l](2,0.25){\fontspec{Open Sans Medium}\scriptsize WAT}
      %\rput[l](6,0.25){\fontspec{Open Sans Medium}\scriptsize WIE}
           \rput[l](6.5,0.25){\fontspec{Open Sans Medium}\scriptsize BIJZONDERHEDEN}
      \end{pspicture}
    }{}

    \newcommand{\opkomst}[5]
    { \ifthenelse{\equal{\value{evenodd}}{1}}{\setcounter{evenodd}{2}}{\setcounter{evenodd}{1}}
      \begin{pspicture}(0,0)(18,0.8)
      \ifthenelse{\equal{\value{evenodd}}{1}}
      { \psframe[cornersize=absolute,%
                 linearc=0.2,%
                 fillstyle=solid,%
                 linecolor=RowColourTwo,%
                 fillcolor=RowColourTwo](-.5,0)(17,0.8)}
      { \psframe[cornersize=absolute,%
                 linearc=0.2,%
                 fillstyle=solid,%
                 linecolor=RowColour,%
                 fillcolor=RowColour](-.5,0)(17,0.8)}
      \ifthenelse{\equal{#2}{}}{\def\dPos{.55}}{\def\dPos{.58}}
        \rput[l](-.25,\dPos){\footnotesize #1}
        \rput[l](-.25,.245){\footnotesize #2}
      \rput[l](2,0.55){#3}
      \rput[l](6,0.55){#4}
      \rput[l](9.5,0.55){\begin{minipage}{7cm}#5\end{minipage}}
      \end{pspicture}
    }

    \newcommand{\opkomstW}[4]
    { \ifthenelse{\equal{\value{evenodd}}{1}}{\setcounter{evenodd}{2}}{\setcounter{evenodd}{1}}
      \begin{pspicture}(0,0)(18,0.8)
      \ifthenelse{\equal{\value{evenodd}}{1}}
      { \psframe[cornersize=absolute,%
                 linearc=0.2,%
                 fillstyle=solid,%
                 linecolor=RowColourTwo,%
                 fillcolor=RowColourTwo](-.5,0)(17,0.8)}
      { \psframe[cornersize=absolute,%
                 linearc=0.2,%
                 fillstyle=solid,%
                 linecolor=RowColour,%
                 fillcolor=RowColour](-.5,0)(17,0.8)}
      \ifthenelse{\equal{#2}{}}{\def\dPos{.55}}{\def\dPos{.58}}
        \rput[l](-.25,\dPos){\footnotesize #1}
        \rput[l](-.25,.245){\footnotesize #2}
      \rput[l](2,0.55){#3}
      %\rput[l](6,0.55){#4}
        \rput[l](6.5,0.55){#4}
      \end{pspicture}
    }
    
    %\newenvironment{nest}[2]
    \newcommand{\BeginNest}[2]
    { %\begin{figure}[h]
      \begin{minipage}{16.5cm}
      \@ifundefined{c@evenodd}{\newcounter{evenodd}}{}
      \setcounter{evenodd}{2}
      \gdef\kleur{#1}
      \begin{pspicture}(0,0)(16.5,1.2)
      \psframe[cornersize=absolute,%
                 linearc=0.2,%
                 fillstyle=solid,%
                 linecolor=Nest\kleur 1,%
                 fillcolor=Nest\kleur 1](0,0)(16.5,1.2)
      \rput[r](16.25,0.85){\fontspec{Open Sans Medium}\large\sf\color{White} #2}
      \rput[l](0.25,0.3){\fontspec{Open Sans Medium}\tiny\color{White}\textbf{NAAM}}
      \rput[l](3,0.3){\fontspec{Open Sans Medium}\tiny\color{White}\textbf{ADRES}}
      \rput[l](7.25,0.3){\fontspec{Open Sans Medium}\tiny\color{White}\textbf{TELEFOON}}
      \rput[l](9.75,0.3){\fontspec{Open Sans Medium}\tiny\color{White}\textbf{EMAIL}}
      \rput[l](14,0.3){\fontspec{Open Sans Medium}\tiny\color{White}\textbf{GEBOORTEDATUM}}
      \end{pspicture}
    }
    \newcommand{\EindeNest}
    { \end{minipage} % \end{figure} 
      \vspace{.5cm} }

    %\newenvironment{leidingteam}
    \newcommand{\BeginLeidingteam}
    { % \begin{figure}[h]
      \begin{minipage}{16.5cm}
      \@ifundefined{c@evenodd}{\newcounter{evenodd}}{}
      \setcounter{evenodd}{2}
      \begin{pspicture}(0,0)(16.5,1.2)
      \psframe[cornersize=absolute,%
                 linearc=0.2,%
                 fillstyle=solid,%
                 linecolor=Leiding1,%
                 fillcolor=Leiding1](0,0)(16.5,1.2)
      \rput[r](16.25,0.85){\fontspec{Open Sans Medium}\large\sf\color{White} Leiding}
      \rput[l](0.25,0.3){\fontspec{Open Sans Medium}\tiny\color{White}\textbf{NAAM}}
      \rput[l](3.5,0.3){\fontspec{Open Sans Medium}\tiny\color{White}\textbf{ADRES}}
      \rput[l](7.25,0.3){\fontspec{Open Sans Medium}\tiny\color{White}\textbf{TELEFOON}}
      \rput[l](9.75,0.3){\fontspec{Open Sans Medium}\tiny\color{White}\textbf{EMAIL}}
      \rput[l](14,0.3){\fontspec{Open Sans Medium}\tiny\color{White}\textbf{GEBOORTEDATUM}}
      \end{pspicture}
    }
    \newcommand{\EindeLeidingteam}
    { \end{minipage} % \end{figure} 
      \vspace{.5cm} }

    \newcommand{\welp}[7]
    { \ifthenelse{\equal{\value{evenodd}}{1}}{\setcounter{evenodd}{2}}{\setcounter{evenodd}{1}}
      \def\wHeight{-1}
      \begin{pspicture}(0,\wHeight)(16.5,0)
      \ifthenelse{\equal{\value{evenodd}}{1}}
      { \psframe[cornersize=absolute,%
                 linearc=0.2,%
                 fillstyle=solid,%
                 linecolor=Nest\kleur 3,%
                 fillcolor=Nest\kleur 3](0,\wHeight)(16.5,0)}
      { \psframe[cornersize=absolute,%
                 linearc=0.2,%
                 fillstyle=solid,%
                 linecolor=Nest\kleur 2,%
                 fillcolor=Nest\kleur 2](0,\wHeight)(16.5,0)}
      \rput[tl](0.25,-0.15){\begin{minipage}{2.5cm}\footnotesize\color{Nest\kleur 1}\raggedleft\bfseries#1\vfill\end{minipage}}
      \rput[tl](3,-0.15){\begin{minipage}{4.25cm}\footnotesize#2\vfill\end{minipage}}
      \rput[tl](7.25,-0.15){\begin{minipage}{2.25cm}\footnotesize#3\vfill\end{minipage}}
      \rput[tl](9.75,-0.15){\begin{minipage}{3.75cm}\scriptsize#4\vfill\end{minipage}}
      \rput[tl](14,-0.15){\begin{minipage}{2.35cm}\footnotesize#5\vfill\end{minipage}}
      \lStatus{#7}
      \end{pspicture}\vspace{-.5cm}
    }


    \newcommand{\leiding}[7]
    { \ifthenelse{\equal{\value{evenodd}}{1}}{\setcounter{evenodd}{2}}{\setcounter{evenodd}{1}}
      \ifthenelse{\equal{#6}{}}
        {\def\wHeight{-1}}{\def\wHeight{-1.6}}
      \begin{pspicture}(0,\wHeight)(16.5,0)
      \ifthenelse{\equal{\value{evenodd}}{1}}
      { \psframe[cornersize=absolute,%
                 linearc=0.2,%
                 fillstyle=solid,%
                 linecolor=Leiding3,%
                 fillcolor=Leiding3](0,\wHeight)(16.5,0)}
      { \psframe[cornersize=absolute,%
                 linearc=0.2,%
                 fillstyle=solid,%
                 linecolor=Leiding2,%
                 fillcolor=Leiding2](0,\wHeight)(16.5,0)}
      \rput[tl](0.25,-0.15){\begin{minipage}{3cm}\footnotesize\color{Leiding1}\raggedleft\bfseries#1\vfill\end{minipage}}
      \rput[tl](3.5,-0.15){\begin{minipage}{3.75cm}\footnotesize#2\vfill\end{minipage}}
      \rput[tl](7.25,-0.15){\begin{minipage}{2.25cm}\footnotesize#3\vfill\end{minipage}}
      \rput[tl](9.75,-0.15){\begin{minipage}{3.75cm}\scriptsize#4\vfill\end{minipage}}
      \rput[tl](14,-0.15){\begin{minipage}{2.35cm}\footnotesize#5\vfill\end{minipage}}
      \ifthenelse{\equal{#6}{}}{}
        { \psline[linewidth=.4mm, linecolor=White](.25,-1)(16.25,-1)
          \rput[l](3,-1.25){\begin{minipage}{13.25cm}\scriptsize #6\end{minipage}}}
      \lStatus{#7}
      \end{pspicture}\vspace{-.5cm}
    }

    \newcommand{\lStatus}[1]
    { \ifthenelse{\equal{#1}{O}}
      {\rput[r](-0.1,-0.2){\tiny\sffamily\raggedleft Vliegt over}}
      {\ifthenelse{\equal{#1}{W}}
        {\rput[r](-0.1,-0.2){\tiny\sffamily\raggedleft Gaat weg}}
        {\ifthenelse{\equal{#1}{N}}
          {\rput[r](-0.1,-0.2){\tiny\sffamily\raggedleft Nieuw: Installeren}}
          {\ifthenelse{\equal{#1}{A}}
            {\rput[r](-0.1,-0.2){\tiny\sffamily\raggedleft Wordt Akela}}
            {\ifthenelse{\equal{#1}{M}}
              {\rput[r](-0.1,-0.2){\tiny\sffamily\raggedleft Nieuw (Mogelijk)}}
              {}}}}}}

\makeatletter
\renewcommand \dotfill {\vspace{-1em}\leavevmode \cleaders \hb@xt@ .22em{\hss .\hss }\hfill \kern \z@}
\makeatother

\newcommand\dotline[1]{\leavevmode\hbox to #1{\dotfill}}
